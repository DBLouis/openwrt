# OpenWrt

This repository contains configuration files and scripts to build a custom OpenWrt firmware for the PC Engines APU2 platform.
It is primarily intended for my personal use, but feel free to use it as a starting point for your own custom firmware.

## Features

- LuCI web interface **not** included, only UCI.
- Automatic file system resizing on first boot.
- EXT4 file system with journaling enabled.
- Ad-blocking with `adblock-fast` package.
- DNS-over-TLS and DNSSEC with `unbound` package and `quad9` as upstream resolver.
- Docker container host.
- Monitoring with [collectd](https://www.collectd.org/), [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/).
- Secure non-root SSH access with public key authentication only.
- Router is a [ZeroTier](https://www.zerotier.com/) gateway.
- Compiler target optimized for the APU2 processor (AMD GX-412TC).
- Hardware crypto acceleration enabled for OpenSSL.
- Hardware RNG enabled.
- Secure FTP server.
